package require uuid
package require uri
package require TclCurl
package require snit
package require json
package require json::write

snit::type JSONRPC_Client {
    typevariable responseHeader
    typevariable responseBody
    typevariable bufferError
    
    typemethod uuid { } {
	uuid::uuid generate
    }

    typemethod errorUnexpected { desc id } {
	set objData [list "description" [json::write string $desc]]
	set objError [list "code" -50000 \
			  "message" {"Unexpected Error"} \
			  "data" [json::write object {*}$objData]]
	json::write object "jsonrpc" {"2.0"} \
	    "error" [json::write object {*}$objError] \
	    "id" [json::write string $id]
    }
    
    typemethod errorHTTP { codeHTTP response id } {
	set objData [list "codeHTTP" $codeHTTP "response" [json::write string $response]]
	set objError [list "code" -50001 \
			  "message" {"Unexpected HTTP Code"} \
			  "data" [json::write object {*}$objData]]
	json::write object "jsonrpc" {"2.0"} \
	    "error" [json::write object {*}$objError] \
	    "id" [json::write string $id]
    }
    
    typemethod invoke {uri route method args} {
	array set opts {
	    -params ""
	    -authToken ""
	    -verbose 0
	}
	array set uriParts [uri::split $uri]
	if {$uriParts(host) eq ""} {
	    error "empty host provided"
	}
	set url [uri::join scheme $uriParts(scheme) \
		     host $uriParts(host) \
		     path $route]
	array set opts $args
	if {$opts(-params) eq ""} {
	    set kvParams ""
	} else {
	    set kvParams [list "params" $opts(-params)]
	}
	set id [uuid::uuid generate]
	set reqRPC [json::write object "jsonrpc" {"2.0"} \
			"method" [json::write string $method] {*}$kvParams \
			"id" [json::write string $id]]
	
	set curlHandle [curl::init]
	
	if {$uriParts(port) eq ""} {
	    set kvPort ""
	} else {
	    set kvPort [list -port $uriParts(port)]
	}
	if {$opts(-authToken) eq ""} {
	    set kvAuthToken ""
	} else {
	    set kvAuthToken [list -httpheader [list "Authorization: Bearer $opts(-authToken)"]]
	}
	$curlHandle configure -url $url \
	    {*}$kvPort \
	    -verbose [expr {$opts(-verbose)?1:0}] \
	    -httpheader [list "Content-Type: application/json" ] \
	    {*}$kvAuthToken \
	    -postfields $reqRPC \
	    -errorbuffer [mytypevar bufferError] \
	    -headervar [mytypevar responseHeader] \
	    -bodyvar [mytypevar responseBody]
	if {[catch {$curlHandle perform} status]} {
	    puts "capturado error status = $status"
	    puts "bufferError = $bufferError"
	}
	if {$status} {
	    set result [$type errorUnexpected $bufferError $id] 
	} else {
	    set httpCode [$curlHandle getinfo httpcode]
	    if {$httpCode == 200} {
		set result $responseBody
	    } else {
		set result [$type errorHTTP $httpCode $responseBody $id]
	    }
	}
	$curlHandle cleanup
	return $result
    }
}
